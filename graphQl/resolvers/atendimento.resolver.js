const operations = require('../../infraestrutura/operations')
const Atendimento = new operations('atendimento')

const resolvers = {
    Query: {
        Atendimentos: () => Atendimento.lista(),
        Atendimento: (root, { id }) => Atendimento.buscaPorId(id)
    },
    Mutation: {
        AdicionarAtendimento: (root, params) => Atendimento.adiciona(params),
        AtualizarAtendimento: (root, params) => Atendimento.atualiza(params),
        DeletarAtendimento: (root, { id }) => Atendimento.deleta(id)
    }
}

module.exports = resolvers;