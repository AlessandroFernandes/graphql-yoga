const operations = require('../../infraestrutura/operations')
const Cliente = new operations('cliente')

const resolvers = {
    Query : {
      Clientes: () => Cliente.lista(),
      Cliente: (root, { id }) => Cliente.buscaPorId(id),
    },
    Mutation: {
      AdicionarCliente: (root, params) => Cliente.adiciona(params),
      DeletarCliente: (root, { id }) => Cliente.deleta(id),
      AtualizarCliente: (root, params) => Cliente.atualiza(params),
    }
  }

  module.exports = resolvers;