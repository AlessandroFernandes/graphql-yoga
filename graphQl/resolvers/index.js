const path = require('path');
const mergeGraphqlSchemas = require('merge-graphql-schemas');

const files = path.join(__dirname, './');

const { fileLoader } = mergeGraphqlSchemas;

const resolvers = fileLoader(files);

module.exports = resolvers;

