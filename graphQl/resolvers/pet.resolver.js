const operations = require('../../infraestrutura/operations')

const Pet = new operations('pet')

const resolvers = {
    Query : {
      Pets: () => Pet.lista(),
      Pet: (root, { id }) => Pet.buscaPorId(id)
    },
    Mutation: {
      AdicionarPet: (root, params) => Pet.adiciona(params),
      DeletarPet: (root, { id }) => Pet.deleta(id),
      AtualizarPet: (root, params) => Pet.atualiza(params)
    }
  }

  module.exports = resolvers;