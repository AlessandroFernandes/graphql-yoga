const operations = require('../../infraestrutura/operations')

const Servico = new operations('servico')

const resolvers = {
    Query : {
        Servicos: () => Servico.lista(),
        Servico: (root, { id }) => Servico.buscaPorId(id)
    },
    Mutation: {
        AdicionarServico: (root, params) => Servico.adiciona(params),
        AtualizarServico: (root, params) => Servico.atualiza(params),
        DeletaServico: (root, { id }) => Servico.deleta(id)
    }
}

module.exports = resolvers;

