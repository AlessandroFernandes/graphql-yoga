const path = require('path');
const mergeGraphqlSchemas = require('merge-graphql-schemas');

const files = path.join(__dirname, './');

const {
    fileLoader,
    mergeTypes
} = mergeGraphqlSchemas;

const mFile = fileLoader(files);
const schema = mergeTypes(mFile);

module.exports = schema;