const executaQuery = require('../database/queries')

class Atendimento {
  lista() {
    const sql =  `SELECT * FROM Atendimentos;
                  SELECT * FROM Clientes;
                  SELECT * FROM Pets;
                  SELECT * FROM Servicos`

    return executaQuery(sql).then(data => {

      const atendimento = data[0];
      const cliente = data[1];
      const pet = data[2];
      const servicos = data[3];

      return atendimento.map(at => {

        const cl = cliente.filter(cl => at.clienteId === cl.id);
        const pe = pet.filter(pt => at.petId === pt.id);
        const sv = servicos.filter(sv => at.servicoId === sv.id);

        console.log(cl);

        return ({
          ...at,
          cliente: cl[0],
          pet: pe[0],
          servico: sv[0]
        })
      
      })
    })
  }

  buscaPorId(id) {
    const sql = `SELECT Atendimentos.id, Atendimentos.data, Atendimentos.status, Atendimentos.status, Atendimentos.observacoes,
                      Clientes.id as clienteId, Clientes.nome as clienteNome, Clientes.cpf as clienteCpf,
                      Pets.id as petsId, Pets.nome as petsNome, Pets.tipo as petsTipo, Pets.observacoes as petsObservacoes,
                      Servicos.id as servicoId, Servicos.nome as servicoNome, Servicos.preco as servicoPreco, Servicos.descricao as servicoDescricao
                  FROM Atendimentos
                  INNER JOIN Clientes
                  INNER JOIN Pets
                  INNER JOIN Servicos
                  WHERE Atendimentos.id=${id} 
                  AND Atendimentos.clienteId = Clientes.id
                  AND Atendimentos.petId = Pets.id
                  AND Atendimentos.servicoId = Servicos.id`;

    return executaQuery(sql).then(data => ({
      id: data[0].id,
      data: data[0].data,
      status: data[0].status,
      observacoes: data[0].observacoes,
      cliente: {
        id: data[0].clienteId,
        nome: data[0].clienteNome,
        cpf: data[0].clienteCpf
      },
      pet: {
        id: data[0].petsId,
        nome: data[0].petsNome,
        tipo: data[0].petsTipo,
        observacoes: data[0].petsObservacoes
      },
      servico: {
        id: data[0].servicoId,
        nome: data[0].servicoNome,
        preco: data[0].servicoPreco,
        descricao: data[0].servicoDescricao
      }
    }))
  }

  adiciona(item) {
    const { cliente, pet, servico, status, observacoes } = item
    const date = new Date().toLocaleDateString()

    const sql = `INSERT INTO Atendimentos(clienteId, petId, servicoId, data, status, observacoes) VALUES(${cliente}, ${pet}, ${servico}, '${date}', '${status}', '${observacoes}');
                 SELECT * FROM Clientes WHERE id=${cliente};
                 SELECT * FROM Pets WHERE id=${pet};
                 SELECT * FROM Servicos WHERE id=${servico}`

    return executaQuery(sql).then(data => {

      const atendimento = data[0];
      const cliente = data[1][0];
      const pet = data[2][0];
      const servico = data[3][0];

      return ({
        id: atendimento.insertId,
        cliente: {
          id: cliente.id,
          nome: cliente.nome,
          cpf: cliente.cpf
        },
        pet: {
          id: pet.id,
          nome: pet.nome,
          tipo: pet.tipo,
          observacoes: pet.observacoes
        },
        servico: {
          id: servico.id,
          nome: servico.nome,
          preco: servico.preco,
          descricao: servico.descricao
        },
        data: date,
        status,
        observacoes
      })
    })
  }

  atualiza(novoItem) {
    const { id, cliente, pet, servico, status, observacoes } = novoItem
    const data = new Date.toLocaleDateString()
  
    const sql = `UPDATE Atendimentos SET clienteId=${cliente}, petId=${pet}, servicoId=${servico}, data='${data}', status='${status}' observacoes='${observacoes}' WHERE id=${id};
                 SELECT * FROM Clientes WHERE id=${cliente};
                 SELECT * FROM Pets WHERE id=${pet};
                 SELECT * FROM Servicos WHERE id=${servico}`

    executaQuery(sql).then(data => {
     
      const cliente = data[1][0];
      const pet = data[2][0];
      const servico = data[3][0];

      return ({
        id,
        status,
        observacoes,
        cliente: {
          id: cliente.id,
          nome: cliente.nome,
          cpf: cliente.cpf
        },
        pet: {
          id: pet.id,
          nome: pet.nome,
          tipo: pet.tipo,
          observacoes: pet.observacoes
        },
        servico: {
          id: servico.id,
          nome: servico.nome,
          preco: servico.preco,
          descricao: servico.descricao
        }
      })
    })
  }

  deleta(id) {
    const sql = `DELETE FROM Atendimentos WHERE id=${id}`

    return executaQuery(sql).then(() => id)
  }
}

module.exports = new Atendimento
