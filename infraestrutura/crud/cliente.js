const executaQuery = require('../database/queries')

class Cliente {

  lista() {
    const sql = 'SELECT * FROM Clientes; SELECT * FROM pets'

    return executaQuery(sql).then(data => {
      const clientes = data[0];
      const pets = data[1];

      return clientes.map(cliente => {

        const petsSelecao = pets.filter(pet => pet.donoId === cliente.id);

        return ({
          ...cliente,
          pets: petsSelecao
        })
      })
    })
  }

  buscaPorId(id) {
    const sql = `SELECT * FROM Clientes WHERE id=${id};
                 SELECT * FROM Pets WHERE donoId=${id}`

    return executaQuery(sql).then(data => {
      const cliente = data[0][0];
      const pets = data[1]

      return ({
        ...cliente,
        pets
      })
    })
  }

  adiciona(item) {
    const { nome, cpf } = item
    const sql = `INSERT INTO Clientes(nome, CPF) VALUES('${nome}', '${cpf}')`

    return executaQuery(sql).then(data =>
       ({
          id: data.insertId,
          nome,
          cpf
       })
    ) 
  }

  atualiza(novoItem) {
    const { nome, cpf, id } = novoItem
    const sql = `UPDATE Clientes SET nome='${nome}', CPF='${cpf}' WHERE id=${id}`

    return executaQuery(sql).then(() => novoItem)
  }

  deleta(id) {
    const sql = `DELETE FROM Clientes WHERE id=${id}`

    return executaQuery(sql).then(() => id)
  }
}

module.exports = new Cliente
