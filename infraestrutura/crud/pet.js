const executaQuery = require('../database/queries')

class Pet {
  lista() {
    const sql = `SELECT Pets.id, Pets.nome, Pets.tipo, Pets.observacoes, 
                 Clientes.id as clienteId, Clientes.nome as clienteNome, Clientes.cpf as clienteCpf
                 FROM Pets 
                 INNER JOIN Clientes
                 WHERE Pets.donoId = Clientes.Id`

    return executaQuery(sql).then(data => 
      data.map(pet => ({
        id: pet.id,
        nome: pet.nome,
        tipo: pet.tipo,
        observacoes: pet.observacoes,
        dono: {
          id: pet.clienteId,
          nome:  pet.clienteNome,
          cpf: pet.clienteCpf
        }
      }))
    )
  }

  buscaPorId(id) {
    const sql = `SELECT  Pets.id, Pets.nome, Pets.tipo, Pets.observacoes,
                 Clientes.id as clienteId, Clientes.nome as clienteNome, Clientes.cpf as clienteCpf
                 FROM Pets
                 INNER JOIN Clientes
                 WHERE Pets.id=${parseInt(id)} AND Clientes.id = Pets.donoId` 

    return executaQuery(sql).then(data => ({
        id: data[0].id,
        nome: data[0].nome,
        tipo: data[0].tipo,
        observacoes: data[0].observacoes,
        dono: {
          id: data[0].clienteId,
          nome: data[0].clienteNome,
          cpf: data[0].clienteCpf
        },
      }))
  }

  adiciona(item) {
    const { nome, dono, tipo, observacoes } = item

    const sql = `INSERT INTO Pets(nome, donoId, tipo, observacoes) VALUES('${nome}', ${dono}, '${tipo}', '${observacoes}')`

    return executaQuery(sql).then(data => ({
      id: data.insertId,
      nome,
      donoId,
      tipo,
      observacoes
    }))
  }

  atualiza(novoItem) {
    const {id, nome, dono, tipo, observacoes } = novoItem

    const sql = `UPDATE Pets SET nome='${nome}', donoId=${dono}, tipo='${tipo}', observacoes='${observacoes}' WHERE id=${id};
                 SELECT * FROM Clientes WHERE id=${parseInt(dono)}`

    return executaQuery(sql).then(data =>{
      
      const dono = data[1][0]

      return ({
        id,
        nome,
        tipo,
        observacoes,
        dono: {
          id: dono.id,
          nome: dono.nome,
          cpf: dono.cpf
        }
      })
    })
  }

  deleta(id) {
    const sql = `DELETE FROM Pets WHERE id=${id}`

    return executaQuery(sql).then(() => id)
  }
}

module.exports = new Pet
