const conexao = require('../conexao')

const executaQuery = query => {
    return new Promise((resolver, reject) => {
      console.log('executou a query!')
      conexao.query(query, (erro, resultados, campos) => {
      if (erro) {
        return reject(erro)
      } else {
        return resolver(resultados)
      }

    })
  })
}

module.exports = executaQuery
