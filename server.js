const { GraphQLServer } = require('graphql-yoga');
const conexao = require('./infraestrutura/conexao')
const Tabelas = require('./infraestrutura/database/tabelas');
const typeDefs = require('./graphQl/schemas/index');
const resolvers = require('./graphQl/resolvers/index');

const app = new GraphQLServer({
  resolvers,
  typeDefs
});

conexao.connect(erro => {
  if (erro) {
    console.log(erro)
  }

  console.log('conectou no banco')

  Tabelas.init(conexao)
})

app.start(() => console.log("Server start"));